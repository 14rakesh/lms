package ProjectActivity;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestCase6 {
	WebDriver driver;
	@BeforeMethod
	public void initiatedriver() {		
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/lms");		 
	}
	@Test
	public void LoginToLMS() {
		WebElement menu_MyAccount = driver.findElement(By.id("menu-item-1507"));
		menu_MyAccount.click();		
		String titleText = driver.getTitle();
		WebElement button_Login = driver.findElement(By.xpath("//a[@class='ld-login ld-login ld-login-text ld-login-button ld-button']"));
		button_Login.click();
		WebElement edit_user = driver.findElement(By.id("user_login"));
		WebElement edit_password = driver.findElement(By.id("user_pass"));
		WebElement button_submit = driver.findElement(By.id("wp-submit"));
		Assert.assertEquals(titleText, "My Account � Alchemy LMS");
		edit_user.sendKeys("root");
		edit_password.sendKeys("pa$$w0rd");
		button_submit.click();
		WebElement link_EditProfile = driver.findElement(By.className("ld-profile-edit-link"));
		Assert.assertTrue(link_EditProfile.isDisplayed());
		
	}
	@AfterMethod
	public void close() {
		driver.quit();
	}
}
