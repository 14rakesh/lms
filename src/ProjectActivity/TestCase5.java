package ProjectActivity;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestCase5 {
	WebDriver driver;
	@BeforeMethod
	public void initiatedriver() {		
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/lms");		 
	}
	@Test
	public void selectMyAccount() {
		WebElement menu_MyAccount = driver.findElement(By.id("menu-item-1507"));
		menu_MyAccount.click();
		
		String titleText = driver.getTitle();
		Assert.assertEquals(titleText, "My Account � Alchemy LMS");
	}
	@AfterMethod
	public void close() {
		driver.quit();
	}
}
