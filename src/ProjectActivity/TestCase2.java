package ProjectActivity;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestCase2 {
	WebDriver driver;
	@BeforeMethod
	public void initiatedriver() {		
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/lms");		 
	}
	@Test
	public void VerifyHeading() {
		WebElement Static_header = driver.findElement(By.xpath("//h1[@class='uagb-ifb-title']"));
		String header = Static_header.getText();
		Assert.assertEquals(header, "Learn from Industry Experts");
	}
	@AfterMethod
	public void close() {
		driver.quit();
	}
}
