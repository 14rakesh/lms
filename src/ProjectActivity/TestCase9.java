package ProjectActivity;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestCase9 {
	WebDriver driver;
	@BeforeMethod
	public void initiatedriver() {		
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/lms");		 
	}
	
	@Test(priority=1)
	public void MarkCompleteALesson() {
		WebElement menu_MyAccount = driver.findElement(By.id("menu-item-1507"));
		menu_MyAccount.click();		
		String titleText = driver.getTitle();
		WebElement button_Login = driver.findElement(By.xpath("//a[@class='ld-login ld-login ld-login-text ld-login-button ld-button']"));
		button_Login.click();
		WebElement edit_user = driver.findElement(By.id("user_login"));
		WebElement edit_password = driver.findElement(By.id("user_pass"));
		WebElement button_submit = driver.findElement(By.id("wp-submit"));
		Assert.assertEquals(titleText, "My Account � Alchemy LMS");
		edit_user.sendKeys("root");
		edit_password.sendKeys("pa$$w0rd");
		button_submit.click();
		WebElement link_EditProfile = driver.findElement(By.className("ld-profile-edit-link"));
		Assert.assertTrue(link_EditProfile.isDisplayed());
		
		WebElement menu_AllCourses = driver.findElement(By.id("menu-item-1508"));
		menu_AllCourses.click();
		List<WebElement> listofCourse = driver.findElements(By.xpath("//a[@class='btn btn-primary']"));
		//click the first course
		listofCourse.get(0).click();
		List<WebElement> listOfLessons = driver.findElements(By.xpath("//div[@class='ld-item-title']"));
		listOfLessons.get(0).click();
		WebElement lessonTitle = driver.findElement(By.xpath("//div[@class='ld-focus-content']/h1")); 
		String titleAct = lessonTitle.getText();
		Assert.assertEquals(titleAct, "Developing Strategy");
		System.out.println(titleAct);
		WebElement button_MarkComplete = driver.findElement(By.xpath("//input[@class='learndash_mark_complete_button']")); 
	}
	@AfterMethod
	public void close() {
		driver.quit();
	}
}
