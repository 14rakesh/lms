package ProjectActivity;

import static org.testng.Assert.assertEquals;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestCase7 {
	WebDriver driver;
	@BeforeMethod
	public void initiatedriver() {		
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/lms");		 
	}
	@Test
	public void countTheCourses() {
		WebElement menu_AllCourses = driver.findElement(By.id("menu-item-1508"));
		menu_AllCourses.click();
		List<WebElement> listofCourse = driver.findElements(By.xpath("//div[@class='ld_course_grid col-sm-8 col-md-4 ']"));
		listofCourse.size();
		System.out.println("Number of courses "+listofCourse.size());
	}
	@AfterMethod
	public void close() {
		driver.quit();
	}
}
