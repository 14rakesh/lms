package ProjectActivity;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestCase8 {
	WebDriver driver;
	@BeforeMethod
	public void initiatedriver() {		
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/lms");		 
	}
	@Test
	public void submitComment() {
		WebElement menu_Contact = driver.findElement(By.id("menu-item-1506"));
		menu_Contact.click();
		WebElement edit_FullName = driver.findElement(By.id("wpforms-8-field_0"));
		WebElement edit_Email = driver.findElement(By.id("wpforms-8-field_1"));
		WebElement edit_Subject = driver.findElement(By.id("wpforms-8-field_3"));
		WebElement edit_Comment = driver.findElement(By.id("wpforms-8-field_2"));
		WebElement edit_Submit = driver.findElement(By.xpath("//button[@id='wpforms-submit-8']"));
		edit_FullName.sendKeys("Rakesh Kumar");
		edit_Email.sendKeys("rakekumn@in.ibm.com");
		edit_Subject.sendKeys("test contact");
		edit_Comment.sendKeys("Input the comment");
		edit_Submit.click();
		WebElement static_message = driver.findElement(By.xpath("//div[@id='wpforms-confirmation-8']"));
		String msgActual = static_message.getText();
		System.out.println("Confirmation message is "+msgActual);
	}
	@AfterMethod
	public void close() {
		driver.quit();
	}
}
