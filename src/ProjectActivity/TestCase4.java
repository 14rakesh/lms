package ProjectActivity;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestCase4 {
	WebDriver driver;
	@BeforeMethod
	public void initiatedriver() {		
		driver = new FirefoxDriver();
		driver.get("https://alchemy.hguy.co/lms");		 
	}
	@Test
	public void GetSecondPopularCourse() {
		WebElement popularCourseTitle = driver.findElement(By.xpath("//h3[text()='Email Marketing Strategies']"));
		String titleText = popularCourseTitle.getText();
		Assert.assertEquals(titleText, "Email Marketing Strategies");
	}
	@AfterMethod
	public void close() {
		driver.quit();
	}
}
